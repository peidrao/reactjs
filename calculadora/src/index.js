import React from 'react';
import ReactDOM from 'react-dom';
import './App.css';
import Calculator from './main/calc';

ReactDOM.render(
  <div>
    <h1>Calculadora</h1>
    <Calculator />
  </div>,
  document.getElementById('root')
);
